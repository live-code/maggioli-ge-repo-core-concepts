import { Component } from '@angular/core';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-users',
  template: `
    <div class="container mt-3">
      <div class="alert alert-danger" *ngIf="userService.error">C'è un errore</div>
      
      <form #f="ngForm" (submit)="userService.save(f)">
        
        <div *ngIf="inputName.errors?.required">Il campo è obbligatorio</div>
        <div *ngIf="inputName.errors?.minlength">
          Il campo è tropppo corto. Mancano {{inputName.errors?.minlength.requiredLength - inputName.errors?.minlength.actualLength}} chars
        </div>
        <input 
          class="form-control" 
          [ngClass]="{'is-invalid': inputName.invalid && f.dirty, 'is-valid': inputName.valid }" 
          #inputName="ngModel"
          [ngModel]="userService.activeUser?.name"
          required 
          minlength="5"
          type="text" name="name" 
        >
        <app-input-bar [field]="inputName"></app-input-bar>
        
        
        <input 
          class="form-control"
          [ngClass]="{'is-invalid': inputCity.invalid && f.dirty, 'is-valid': inputCity.valid}" 
          #inputCity="ngModel" 
          type="text" name="city"
          [ngModel]="userService.activeUser?.city"
          required minlength="3"
        >
        <app-input-bar  [field]="inputCity"></app-input-bar>
       
        <select name="gender"
                [ngModel]="userService.activeUser?.gender"
                #inputGender="ngModel" 
                class="form-control" required
                [ngClass]="{'is-invalid': inputGender.invalid && f.dirty, 'is-valid': inputGender.valid}" 
        >
          <option [ngValue]="null">Select gender</option>
          <option value="M">male</option>
          <option value="F">female</option>
        </select>

        <button type="submit" [disabled]="f.invalid">
          {{userService.activeUser ? 'EDIT' : 'ADD'}}
        </button>
        <button type="button"  (click)="userService.clearForm(f)">CLEAR</button>
      </form>
      
      <li 
        *ngFor="let user of userService.users; let i = index; let last = last"
        (click)="userService.setActiveHandler(user)"
        class="list-group-item"
        [ngClass]="{'female': user.gender === 'F', 'male': user.gender === 'M'}"
        [style.border-bottom]="last ? '3px solid black' : null"
        [style.border]="user.id === userService.activeUser?.id ? '3px solid black' : null"
      >

        {{i+1}} - {{user.name}} - {{user.id}}
        <i class="fa fa-trash pull-right" 
           (click)="userService.deleteHandler(user.id, $event)"></i>
      </li>
    </div>
    
    {{update()}}
  `,
})
export class UsersComponent {
  constructor(public userService: UserService) {
    this.userService.getUsers();
  }

  update(): void {
    console.log('update')
  }
}
