import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <app-navbar></app-navbar>
    <hr>
    <div style="margin: 0 30px">
      <router-outlet></router-outlet>
    </div>
  `,

})
export class AppComponent {
}
