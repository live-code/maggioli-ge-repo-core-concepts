import { Injectable } from '@angular/core';
import { User } from '../../../model/user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NgForm } from '@angular/forms';

@Injectable({ providedIn: 'root' })
export class UserService {
  users: User[];
  activeUser: User;
  error = false;

  constructor(private http: HttpClient ) {}

  getUsers(): void {
    this.error = false;
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe(
        (result) => this.users = result,
        err => this.error = true
      );
  }

  deleteHandler(id: number, event: MouseEvent): void {
    event.stopPropagation();
    this.error = false;
    this.http.delete('http://localhost:3000/users/' + id)
      .subscribe(
        () => {
          this.users = this.users.filter(u => u.id !== id);
          if (this.activeUser?.id === id) {
            this.activeUser = null;
          }
        },
        err => this.error = true
      );
  }

  save(form: NgForm): void {

    if (this.activeUser) {
      this.edit(form);
    } else {
      this.add(form);
    }
  }

  add(form: NgForm): void {
    this.http.post<User>('http://localhost:3000/users/', form.value)
      .subscribe(res => {
        this.users.push(res)
        form.reset();
      })
  }

  edit(form: NgForm): void {
    this.http.patch<User>('http://localhost:3000/users/' + this.activeUser.id, form.value)
      .subscribe(res => {
        const index = this.users.findIndex(u => u.id === this.activeUser.id);
        this.users[index] = res;
      });
  }

  setActiveHandler(user: User): void {
    console.log('set active')
    this.activeUser = user;
  }

  clearForm(f: NgForm): void {
    f.reset();
    this.activeUser = null;
  }
}
