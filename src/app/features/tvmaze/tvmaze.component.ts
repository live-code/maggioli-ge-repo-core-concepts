import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Series } from '../../model/series';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-tvmaze',
  template: `
   
    <h1>TV MAZE</h1>
    <form #f="ngForm" (submit)="submit(f)">
      <input type="text" ngModel name="text" required="required" placeholder="Search series">
      <button type="submit">Send</button>
    </form>
    
    
    <li *ngFor="let series of result">
      {{series.show.name}}
      <img [src]="series.show.image?.medium" alt="" width="100">
      <div *ngIf="!series.show.image">NO FOTO</div>
    </li>
  `,

})
export class TvmazeComponent {
  result: Series[];

  constructor(private http: HttpClient) {

  }

  submit(form: NgForm): void {
    this.http.get<Series[]>('http://api.tvmaze.com/search/shows?q=' + form.value.text)
      .subscribe(res => {
        this.result = res;
      });
  }
}
