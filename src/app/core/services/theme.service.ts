import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ThemeService {
  theme = 'dark';

  setTheme(val: string): void {
    this.theme = val;
  }
}
