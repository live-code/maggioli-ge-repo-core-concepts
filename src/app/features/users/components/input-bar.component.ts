import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-input-bar',
  template: `
    <div
      class="bar"
      [style.width.%]="getPerc(field?.errors?.minlength)"
    ></div>
  `,
  styles: [`
    .bar {
      background-color: orange;
      height: 15px;
      transition: 1s all ease-in-out;
    }
  `]
})
export class InputBarComponent {
  @Input() field: any;

  getPerc(minlength: any): number {
    if (minlength) {
      return (minlength?.actualLength / minlength?.requiredLength) * 100
    }
    return 0;
  }
}
