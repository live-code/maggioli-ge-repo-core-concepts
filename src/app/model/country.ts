export interface Country {
  id: number;
  name: string;
  desc: string;
  cities: City[];
}

export interface City {
  id: number;
  label: string;
}
