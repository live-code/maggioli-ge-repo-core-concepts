import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'app-settings',
  template: `
    <h1>ciao ciao settings</h1>
    
    <button 
      (click)="themeService.theme = 'dark'"
      [style.backgroundColor]="themeService.theme === 'dark' ? 'yellow' : null"
    >dark</button>
    
    <button 
      (click)="themeService.setTheme('light')"
      [style.backgroundColor]="themeService.theme === 'light' ? 'yellow' : null"
    >light</button>
    <pre>{{themeService.theme}}</pre>
  `,
})
export class SettingsComponent implements OnInit {

  constructor(public themeService: ThemeService) {}

  ngOnInit(): void {
  }

}
