import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  template: `

    <div class="card">
      <div 
        class="card-header" 
        (click)="open = !open"
      >
        {{title}}
        <i class="pull-right" [ngClass]="icon"
           (click)="iconClickHandler($event)"></i>
      </div>
      <div class="card-body" *ngIf="open">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input() title: string;
  @Input() icon: string;
  @Output() iconClick: EventEmitter<void> = new EventEmitter();
  open = true;

  iconClickHandler(event: MouseEvent): void {
    event.stopPropagation();
    this.iconClick.emit();
  }
}
