import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-gmap',
  template: `
    <div style="border: 1px solid black">
      <div style="padding: 20px">
        <h4>{{city}}</h4>
        <img [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + city + '&zoom=5&size=200x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'" alt="">
      </div>
    </div>
  `,
})
export class GmapComponent {
  @Input() city = 'Rome';
}
