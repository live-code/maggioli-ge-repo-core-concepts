import { Component, OnInit } from '@angular/core';
import { City, Country } from '../../model/country';

@Component({
  selector: 'app-uikit',
  template: `
    <app-tabbar
      [items]="countries" 
      [active]="selectedCountry"
      (tabClick)="setActiveCountry($event)"
    ></app-tabbar>

    <app-tabbar
      [items]="selectedCountry?.cities"
      labelField="label"
      (tabClick)="setActiveCity($event)"
      [active]="selectedCity"
    ></app-tabbar>

    <app-card 
      *ngIf="selectedCity"
      [title]="selectedCity?.label" 
      icon="fa fa-link"
      (iconClick)="openUrl('https://it.wikipedia.org/wiki/' + selectedCity?.label)"
    >
      <app-gmap [city]="selectedCity?.label"></app-gmap>
    </app-card>

    
  `,
})
export class UikitComponent {
  countries: Country[];
  selectedCountry: Country;
  selectedCity: City;

  constructor() {
    setTimeout(() => {
      this.countries = [
        { 
          id: 100,
          name: 'Italy',
          desc: 'bla bla 1',
          cities: [
            { id: 1, label: 'Rome'},
            { id: 2, label: 'Verona'},
            { id: 3, label: 'Milan'}
          ]
        },
        { 
          id: 200,
          name: 'Spain',
          desc: 'bla bla 2 ',
          cities: [
            { id: 1, label: 'Madrid'}
          ]
        },
        { 
          id: 300,
          name: 'Germany',
          desc: 'bla bla  3',
          cities: [
            { id: 1, label: 'Monaco'},
            { id: 2, label: 'Berlino'},
          ]
        },
      ];

      this.setActiveCountry(this.countries[0]);
    }, 1000)
  }

  setActiveCountry(country: Country): void  {
    this.selectedCountry = country;
    this.selectedCity = country.cities[0];
  }

  setActiveCity(city: City): void {
    this.selectedCity = city;
  }



  openUrl(url: string): void {
    window.open(url)
  }

  doSomething(): void {
    console.log('do something')
  }


}
