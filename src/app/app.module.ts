import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { InputBarComponent } from './features/users/components/input-bar.component';
import { HttpClientModule } from '@angular/common/http';
import { UsersComponent } from './features/users/users.component';
import { TvmazeComponent } from './features/tvmaze/tvmaze.component';
import { RouterModule } from '@angular/router';
import { Error404Component } from './features/error404/error404.component';
import { SettingsComponent } from './features/settings/settings.component';
import { NavbarComponent } from './core/components/navbar.component';
import { UikitComponent } from './features/uikit/uikit.component';
import { GmapComponent } from './shared/gmap.component';
import { CardComponent } from './shared/card.component';
import { TabbarComponent } from './shared/tabbar.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    TvmazeComponent,
    InputBarComponent,
    Error404Component,
    SettingsComponent,
    NavbarComponent,
    UikitComponent,
    GmapComponent,
    CardComponent,
    TabbarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'users', component: UsersComponent},
      { path: 'tvmaze', component: TvmazeComponent},
      { path: 'settings', component: SettingsComponent},
      { path: 'uikit', component: UikitComponent},
      { path: '', redirectTo: 'users', pathMatch: 'full'},
      { path: '**', component: Error404Component}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
