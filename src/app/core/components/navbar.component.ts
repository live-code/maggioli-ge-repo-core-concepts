import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ThemeService } from '../services/theme.service';

@Component({
  selector: 'app-navbar',
  template: `

    <div 
      class="mynavbar" 
      [ngClass]="{
        'dark': themeService.theme === 'dark', 
        'light': themeService.theme === 'light'
      }"
    >
      <button routerLink="users" routerLinkActive="highlight">USERS</button>
      <button routerLink="tvmaze" routerLinkActive="highlight">TV MAZE</button>
      <button routerLink="settings" routerLinkActive="highlight">SETTINGS</button>
      <button routerLink="uikit" routerLinkActive="highlight">uikit</button>
    </div>
    {{update()}}
  `,
  styles: [`
    .mynavbar {
      padding: 10px;
    }
    .light {
      background-color: lightgrey;
    } 
    .dark {
      background-color: #222;
      color: white;
    }
    .highlight {
      background-color: yellow;
    }
  `]
})
export class NavbarComponent {
  constructor(public themeService: ThemeService) {
  }

  update() {
    console.log('navbar')
  }
}
