import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li 
        class="nav-item"
        *ngFor="let item of items"
        (click)="tabClick.emit(item)"
      >
        <a class="nav-link"
           [ngClass]="{active: item.id === active?.id}"
           aria-current="page">{{item[labelField]}}</a>
      </li>
    </ul>
  `,
})
export class TabbarComponent {
  @Input() items: any[];
  @Input() active: any;
  @Input() labelField = 'name';
  @Output() tabClick: EventEmitter<any> = new EventEmitter<any>()
}
